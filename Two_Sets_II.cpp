//______________________G R I N D_____A L L____D A Y____L O N G________________________________
    
#include<bits/stdc++.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#define pb push_back
#define all(x) (x).begin(), (x).end()
#define ll long long int
#define ld long double
#define forn(i, n) for (ll i = 0; i < (ll)(n); ++i)
#define fast_cin() ios_base::sync_with_stdio(false); cin.tie(NULL)
#define inarr(arr,n); for(ll i=0;i<n;i++) cin >> arr[i];
#define outarr(arr,n); for(ll i=0;i<n;i++) cout<<arr[i]<<" ";
#define PI 3.141592653589793238462643383279502884197169399375105820974944592307816406286 
using namespace std;
const ll MOD = 1e9+7; // 998244353
const ll INF = 1000000009;
const ll MAXN = 300050;
    
typedef vector<ll> vll;
using namespace std;


void solve()
{
    ll n;
    cin>>n;
    vector<ll> a(n);
    for(ll i=0;i<n;i++) a[i] = i+1;

    ll total = (n*(n+1))/2;
    ll subset = total/2;
    if(total %2!=0)
    {
        cout<<0<<endl;
        return;
    }
    ll dp[n+1][subset+1];
    for(ll i=0;i<=n;i++)
    {
        for(ll j=0;j<=subset;j++)
        {
            if(i==0)
            {
                dp[i][j] = 0;
            }
            if(j==0)
            {
                dp[i][j] = 1;
            }
        }
    }


    for(ll i=1;i<=n;i++)
    {
        for(ll j=1;j<=subset;j++)
        {
            if(a[i-1]<=j)
            {
                dp[i][j] = ((dp[i-1][j])%MOD + (dp[i-1][j-a[i-1]])%MOD)%MOD;
            }
            else dp[i][j] = (dp[i-1][j])%MOD;
        }
    }
    // for(ll i=0;i<=n;i++)
    // {
    //     for(ll j=0;j<=subset;j++)
    //     {
    //         cout<<dp[i][j]<<" ";
    //     }
    //     cout<<endl;
    // }
    cout<<((dp[n][subset] * 500000004)%MOD)<<endl;
} 


int main()
{
    fast_cin();
    ll t=1;
    // cin>>t;

    while(t--)
    {
        solve();
    }  
}
    
    