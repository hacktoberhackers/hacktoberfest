#include <bits/stdc++.h>

using namespace std;

#pragma region
// clang-format off
#define fastIO ios_base::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
#define int long long
#define newLine cout<<"\n";
#define nlc '\n'
#define fullLength(v) v.begin(), v.end()
#define rfullLength(v) v.rbegin(), v.rend()
#define fileIO freopen("hci", "r", stdin); freopen("hco", "w", stdout);
#define readOnly freopen("input.txt", "r", stdin);

const int maxx = 1e18 + 7;
const int cmod = 1e9 + 7;
const string yes = "YES", no = "NO";

int lcm(int a, int b) {
  return (a * b) / __gcd(a, b);
}

bool isPrime(int num) {
  if (num < 2)
    return false;
  for (int x = 2; x * x <= num; x++) {
    if (num % x == 0)
      return false;
    }
  return true;
}

auto cleanRemove = [](vector<int> &vx, int num) {
  return vx.erase(remove(fullLength(vx), num), vx.end());
};

// clang-format on
#pragma endregion

auto safeCeil = [](int a, int b) -> int {
  return ((a == b and a == 1) ? 1 : (a + b - 1) / b);
};

struct coord {
  int val, wt, ind;
};

void print_output(auto lres, auto ltc) {
  //   string res = to_string(lres);
  //   string tc = to_string(ltc);
  const string base = "Case #" + to_string(ltc) + ": ";
  cout << base + to_string(lres) + nlc;
}

const char ca = 'a';
void solve(int tc) {
  string s;
  cin >> s;
  int n = s.size();
  // freq of char till idx i
  vector<vector<int>> stvc(26, vector<int>(n, 0));
  for (int l = 0; l < 26; ++l)
    for (int i = 0; i < n; ++i) {
      stvc[l][i] = stvc[l][max(0LL, i - 1)] + (s[i] == ca + l);
    }
  int q;
  cin >> q;
  int ans = 0;
  while (q--) {
    bool poss = true;
    int l, r;
    cin >> l >> r;
    --l, --r;
    char cod = '*';
    int oc = 0, icod = -1;
    bool to_left = false;
    for (int i = 0; i < 26; ++i) {
      if (stvc[i][r] - (l < 1 ? 0 : stvc[i][l - 1]) & 1) {
        ++oc;
        cod = ca + i;
        to_left = !(stvc[i][r] - stvc[i][(l + r) / 2] >
                    stvc[i][(l + r) / 2 - 1] -
                        (l < 1 ? 0 : stvc[i][l - 1]));
        icod = i;
      }
    }
    if (oc == 1) {
      int mid = (l + r) / 2;
      int ftm, ffm;
      ftm = stvc[icod][mid] - (l < 1 ? 0 : stvc[icod][l - 1]);
      ffm = stvc[icod][r] - (stvc[icod][mid]);
      poss &= abs(ftm - ffm) == 1;
      for (int i = 0; i < 26; ++i) {
        if (ca + i != cod) {
          int fl = stvc[i][mid - !to_left] -
                   (l < 1 ? 0 : stvc[i][l - 1]);
          int fr = stvc[i][r] - stvc[i][mid - !to_left];
          poss &= fl == fr;
        }
      }
      ans += poss;
    }
  }
  print_output(ans, tc);
}

int32_t main() {
  // clang-format off
  fastIO
  #ifdef DEBUG_LOCALE
      readOnly
  #elif !defined (ONLINE_JUDGE)
      fileIO
  #endif

  int t = 1;

  cin >> t;
  int tc = 0;
  while (t > tc++)
    solve(tc);

}
