#include <bits/stdc++.h>

using namespace std;

#pragma region
// clang-format off
#define fastIO ios_base::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
#define int long long
#define newLine cout<<"\n";
#define nlc '\n'
#define fullLength(v) v.begin(), v.end()
#define rfullLength(v) v.rbegin(), v.rend()
#define fileIO freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);

typedef long long ll;

const int maxx = 9e18 + 7;
const string yes = "Yes\n", no = "No\n";

int lcm(int a, int b) {
  return (a * b) / __gcd(a, b);
}

bool isPrime(int num) {
  if (num < 2)
    return false;
  for (int x = 2; x * x <= num; x++) {
    if (num % x == 0)
      return false;
    }
  return true;
}

#define readOnly freopen("input.txt", "r", stdin);

namespace ___debug {
    template <typename T> struct is_outputtable { template <typename C> static constexpr decltype(declval<ostream &>() << declval<const C &>(), bool()) test(int64_t) { return true; } template <typename C> static constexpr bool test(...) { return false; } static constexpr bool value = test<T>(int64_t()); };
    template <class T, typename V = decltype(declval<const T &>().begin()), typename S = typename enable_if<!is_outputtable<T>::value, bool>::type> void pr(const T &x);
 
    template <class T, typename V = decltype(declval<ostream &>() << declval<const T &>())> void pr(const T &x) { cout << x; }
    template <class T1, class T2> void pr(const pair<T1, T2> &x);
    template <class Arg, class... Args> void pr(const Arg &first, const Args &...rest) { pr(first); pr(rest...); }
 
    template <class T, bool pretty = true> void prContain(const T &x) { if (pretty) pr("{"); bool fst = 1; for (const auto &a : x) pr(!fst ? pretty ? ", " : " " : "", a), fst = 0; if (pretty) pr("}"); }
 
    template <class T> void pc(const T &x) { prContain<T, false>(x); pr("\n"); }
    template <class T1, class T2> void pr(const pair<T1, T2> &x) { pr("{", x.first, ", ", x.second, "}"); }
    template <class T, typename V, typename S> void pr(const T &x) { prContain(x); }
    void ps() { pr("\n"); }
    template <class Arg> void ps(const Arg &first) { pr(first); ps(); }
    template <class Arg, class... Args> void ps(const Arg &first, const Args &...rest) { pr(first, " "); ps(rest...); }
}
using namespace ___debug;

#define __pn(x) pr(#x, " = ")
#ifdef DEBUG_LOCALE
#define deb(...) pr("\033[1;31m"), __pn((__VA_ARGS__)), ps(__VA_ARGS__), pr("\033[0m"), cout << flush
#else
#define deb(...)
#endif

#define readOnly freopen("input.txt", "r", stdin);
auto cleanRemove = [](vector<int> &vx, int num) {
  return vx.erase(remove(fullLength(vx), num), vx.end());
};

// clang-format on
#pragma endregion

auto safeCeil = [](int a, int b = 2) -> int {
  return ((a == b and a == 1) ? 1 : (a + b - 1) / b);
};

// cout << fixed << setprecision(10) << stuff;

int csrt(int x) {
  int y = max(0LL, (int)sqrt(x) - 5);
  while (y * y < x) y++;
  if (y * y != x) return -1;
  return y;
}

auto toBin = [](int a) -> int {
  string ans;
  if (a == 0) return 1LL;
  while (a) {
    ans = to_string(a & 1) + ans;
    a >>= 1;
  }
  return stoll(ans);
};

struct coord {
  int x, y;
};

const int mod = 1e18 + 7;
int calcFac(int n) {
  int fac = 1;
  for (int i = n; i > 0; i--) fac *= i, fac %= mod;
  return fac;
}

vector<pair<int, int>> bl(9);

// adjust dims - it consumes memory
int btable[1][1];
int lim = 1e5;
set<int> gsa;
/* Used in https://codeforces.com/contest/192/problem/A */
void pps() {
  for (int i = 1; i < lim; ++i) gsa.insert((i * (i + 1)) / 2);
}

// ring

void print_output(bool res, int tc) {
  string bp = "Suspicious bugs found!";
  const string base = "Scenario #" + to_string(tc) + ':' + nlc +
                      (res ? bp[0] += 32, "No " : "") + bp + nlc;
  cout << base;
}

int exp_bin(int base, int exp) {
  int ans = 1;
  while (exp) {
    ans *= (exp & 1 ? base : 1);
    base *= base;
    exp >>= 1LL;
  }
  return ans;
}

// https://cses.fi/problemset/task/1624 - Chessboard & Queens
const char ca = 'a';
void solve(int tc) {
  vector<string> board(8, "_");
  for (int i = 0; i < 8; ++i) cin >> board[i];
  int ways = 0;
  vector<int> col(8, 0), sum(15, 0), diff(15, 0);

  auto place = [&](auto &&place, int row_num) {
    if (row_num == 8) {
      ++ways;
      return;
    }

    // puts queen on ith col(if poss)
    for (int i = 0; i < 8; ++i) {
      if (board[row_num][i] == '.' and !sum[i + row_num] and !diff[i - row_num + 7] and !col[i]) {
        sum[i + row_num] = diff[i - row_num + 7] = col[i] = true;
        place(place, row_num + 1);
        sum[i + row_num] = diff[i - row_num + 7] = col[i] = false;
      }
    }
  };

  // begin by placing first queen on 0th row
  place(place, 0);
  cout << ways << nlc;
}

int32_t main() {
  // clang-format off
  fastIO
  #ifdef DEBUG_LOCALE
      readOnly
  #elif !defined (ONLINE_JUDGE)
      fileIO
  #endif

  // pps();
  int t = 1;

  // cin >> t;
  int tc = 0;
  // cout << fixed << setprecision(9);
  while (t > tc++)
  solve(tc);

}
